#1 Setup

1. Make www2.plyom.com.br and health.plyom.com.br point to 0.0.0.0 in your internal
proxy.

In mac we can do it by adding the following lines to the end of our /etc/hosts file:

```
0.0.0.0 www2.plyom.com.br
0.0.0.0 health.plyom.com.br
```

Disclaimer! I did not have the time nor energy to research about it.

2. Run:

```
docker-compose build
docker-compose up -d
```

3. Open www2.plyom.com.br (or health.plyom.com.br) at your browser to see the magic.

Points of improvement:
- better way to work with proxies locally.
