$(function() {
    $('#menuToggle, .menu-close').on('click', function() {
        $('#menuToggle').toggleClass('active');
        $('body').toggleClass('body-push-toleft');
        $('#theMenu').toggleClass('menu-open');
    });

    $('#more-project-ei').on('click', function() {
        $('#projectModalEI').modal('toggle');
    });

    $('#more-project-mi').on('click', function() {
        $('#projectModalMI').modal('toggle');
    });
    $('#more-resource').on('click', function() {
        $('#resourceModal').modal('toggle');
    });

    fr = new FilmRoll({
        container: '#film_roll',
        height: 200,
        pager: false,
        prev: true,
        next: true,
        interval: 2500
    });
});
